import serial, datetime, logging, sys, time
from threading import RLock

uuid = "00001101-0000-1000-8000-00805F9B34FB"

class SensorAPI:
    def __init__(self):
        self.ser = serial.Serial()
        #ser.port = sys.argv[1]
        self.ser.port = "/dev/ttyS0"
        self.ser.baudrate = 9600
        self.sensorLock = RLock()

        self.ser.open()
        self.ser.flushInput()
        self.isSleeping = False
        self.neverSleep = False
        self.sensor_sleep()

    def enable_never_sleep(self):
        self.neverSleep = True

    def disable_never_sleep(self):
        self.neverSleep = False
        
    # 0xAA, 0xB4, 0x06, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x06, 0xAB
    def sensor_wake(self):
        print("sensor_wake...")
        with self.sensorLock:
            if(self.isSleeping):
                bytes = [b'\xaa', #head
                b'\xb4', #command 1
                b'\x06', #data byte 1
                b'\x01', #data byte 2 (set mode)
                b'\x01', #data byte 3 (sleep)
                b'\x00', #data byte 4
                b'\x00', #data byte 5
                b'\x00', #data byte 6
                b'\x00', #data byte 7
                b'\x00', #data byte 8
                b'\x00', #data byte 9
                b'\x00', #data byte 10
                b'\x00', #data byte 11
                b'\x00', #data byte 12
                b'\x00', #data byte 13
                b'\xff', #data byte 14 (device id byte 1)
                b'\xff', #data byte 15 (device id byte 2)
                b'\x06', #checksum
                b'\xab'] #tail

                print("sending command...")
                try:
                    for b in bytes:
                        self.ser.write(b)
                    print("command sent")
                    time.sleep(2)
                    self.isSleeping = False
                    print("sensor awoke")
                except:
                    print("error writing wake command")
            else:
                print("sensor already awake")
    
    # xAA, 0xB4, 0x06, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x05, 0xAB
    def sensor_sleep(self):
        print("sensor_sleep...")
            
        with self.sensorLock:
            if (not self.isSleeping):
                if (self.neverSleep):
                    print("neverSleep is True, aborting")
                    return

                bytes = [b'\xaa', #head
                b'\xb4', #command 1
                b'\x06', #data byte 1
                b'\x01', #data byte 2 (set mode)
                b'\x00', #data byte 3 (sleep)
                b'\x00', #data byte 4
                b'\x00', #data byte 5
                b'\x00', #data byte 6
                b'\x00', #data byte 7
                b'\x00', #data byte 8
                b'\x00', #data byte 9
                b'\x00', #data byte 10
                b'\x00', #data byte 11
                b'\x00', #data byte 12
                b'\x00', #data byte 13
                b'\xff', #data byte 14 (device id byte 1)
                b'\xff', #data byte 15 (device id byte 2)
                b'\x05', #checksum
                b'\xab'] #tail
                print("sending command...")
                try:
                    for b in bytes:
                        self.ser.write(b)
                    print("command sent")
                    time.sleep(2)
                    self.isSleeping = True
                    print("sensor sleeping")
                except:
                    print("error writing sleep command")
            else:
                print("sensor already sleeping")

    def sensor_read(self):
        print("sensor_read...")
        print("trying to wake up...")
        self.sensor_wake()
        print("about to read...")
        with self.sensorLock:
            print("reading")
            msg = self.ser.read(size=10)
            #skip all not data msgs
            while(not ((msg[0] == ord(b"\xaa")) and (msg[1] == ord(b"\xc0")) and (msg[9] == ord(b"\xab")))):
                msg = self.ser.read(size=10)
            pm25 = (msg[3] * 256 + msg[2]) / 10.0
            pm10 = (msg[5] * 256 + msg[4]) / 10.0
            return [pm25, pm10]