#!/usr/bin/env python
# -*- coding: utf-8 -*-
import serial, struct, time, csv, datetime, logging, sys, bluetooth, sensor_api
from threading import Thread

uuid = "00001101-0000-1000-8000-00805F9B34FB"

sensor = sensor_api.SensorAPI()

class BluetoothService(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.server_sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
        port = bluetooth.PORT_ANY
        self.server_sock.bind(("",port))
        self.server_sock.listen(1)
        port = self.server_sock.getsockname()[1]
        bluetooth.advertise_service(self.server_sock, "Sample Server", service_id = uuid,
        service_classes = [uuid, bluetooth.SERIAL_PORT_CLASS],
        profiles = [bluetooth.SERIAL_PORT_PROFILE])

    def run(self):
        while True:
            logging.info('waiting for client')
            client_sock,address = self.server_sock.accept()
            print ("Accepted connection from ",address)
            sensor.enable_never_sleep()
            while True:
                try:
                    data = client_sock.recv(1024)
                    if data == b"fetch":
                        try:
                            data = sensor.sensor_read()
                            string = str(data[1])+":"+str(data[0])
                            client_sock.send(string)
                        except:
                            logging.error("sensor error")
                            client_sock.send("sensor error")
                except bluetooth.btcommon.BluetoothError:
                    break #disconnect
            sensor.disable_never_sleep()
            sensor.sensor_sleep()
            client_sock.close()
        self.server_sock.close()

class LoggingService(Thread):
    def __init__(self, frequency, csvfilepath):
        Thread.__init__(self)
        self.frequency = frequency
        self.csvfilepath = csvfilepath

    def writedata(self,data):
        f = open(self.csvfilepath, 'a')
        with f:
            writer = csv.writer(f)
            writer.writerow([str(data[0]), str(data[1]), str(round(datetime.datetime.now().timestamp()))])

    def run(self):
        while True:
            now = time.time()
            data = sensor.sensor_read()
            sensor.sensor_sleep()
            self.writedata(data)
            time.sleep(frequency - (time.time() - now))


if(len(sys.argv) != 3):
    print("usage : [frequency in seconds] [csv file path]")
    sys.exit(-1)
frequency = int(sys.argv[1])
if(frequency < 20):
    error('frequency must be >= 20')
csvfile = sys.argv[2]
    
logging.basicConfig(filename='/home/pi/log.log',level=logging.DEBUG)
logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

btService = BluetoothService()
logService = LoggingService(frequency, sys.argv[2])

logging.info('starting service BT')
btService.start()
logging.info('starting logging service')
logService.start()

btService.join()
logService.join()

